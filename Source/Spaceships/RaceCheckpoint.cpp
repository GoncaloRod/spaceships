#include "RaceCheckpoint.h"

ARaceCheckpoint::ARaceCheckpoint()
{
	PrimaryActorTick.bCanEverTick = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	RootComponent = StaticMesh;

	Trigger = CreateDefaultSubobject<USphereComponent>(TEXT("Trigger Sphere"));
	Trigger->SetupAttachment(RootComponent);

	Trigger->OnComponentBeginOverlap.AddDynamic(this, &ARaceCheckpoint::OnOverlapBegin);
}

void ARaceCheckpoint::BeginPlay()
{
	Super::BeginPlay();

	m_GameMode = GetWorld()->GetAuthGameMode<ATimeAttackGameMode>();

	if (m_GameMode == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Using wrong Game Mode. You must use ATimeAttackGameMode with Race Checkpoints"));
	}

	SetActiveState(bActiveOnStart);

	if (bActiveOnStart)
		m_GameMode->SetStartCheckpoint(this);
}

void ARaceCheckpoint::SetActiveState(bool newState)
{
	m_bIsActive = newState;

	OnActiveStateChanged(newState);
}

void ARaceCheckpoint::OnOverlapBegin(UPrimitiveComponent* overlappedComp, AActor* other, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	if (!m_bIsActive)
		return;

	m_GameMode->CheckpointPassed(this);
}
