#pragma once

#include "CoreMinimal.h"

#include "Engine/DataAsset.h"
#include "Sound/SoundCue.h"

#include "ThrusterDataAsset.generated.h"

/**
 * Data Asset used to store thruster settings.
 * Useful for creating presets of thrusters.
 */
UCLASS()
class SPACESHIPS_API UThrusterDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	/**
	 * Thrust force during normal operation.
	 */
	UPROPERTY(EditAnywhere)
	float StandardThrustForce = 100.f;

	/**
	 * Thrust force during boosting.
	 */
	UPROPERTY(EditAnywhere)
	float BoostingThrustForce = 200.f;

	UPROPERTY(EditAnywhere)
	USoundCue* SoundCue;
	
};
