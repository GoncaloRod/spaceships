#pragma once

#include "CoreMinimal.h"
#include "ShipHUD.h"
#include "GameFramework/Pawn.h"

#include "Camera/CameraComponent.h"

#include "GameFramework/SpringArmComponent.h"

#include "Spaceships/ShipThrusterComponent.h"
#include "Spaceships/ShipInput.h"

#include "Ship.generated.h"

UCLASS()
class SPACESHIPS_API AShip : public APawn
{
	GENERATED_BODY()

public:

	AShip();

	
	virtual void Tick(float deltaTime) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;


	#pragma region Getters/Setters

	/**
	 * Get ship's velocity vector in meters per second.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Stats")
	inline FVector GetShipVelocity() const
	{
		return m_Velocity;
	}

	/**
	 * Get ship's angular velocity in degrees per second.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Stats")
	inline FVector GetShipAngularVelocity() const
	{
		return m_AngularVelocity;
	}

	/**
	 * Get ship's speed in kilometers per hour.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Stats")
	inline float GetSpeedKph() const
	{
		return m_SpeedKph;
	}

	/**
	 * Get ship's speed in meters per second.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Stats")
	inline float GetSpeedMps() const
	{
		return m_SpeedMps;
	}

	/**
	 * Get ship's critical speed.
	 * Exceeding this speed may cause damage to the ship.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Critical Speed")
	inline float GetCriticalSpeed() const
	{
		return CriticalSpeed;
	}

	/**
	 * Get ship's boost tank max capacity.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Boosting")
	inline float GetBoostTankCapacity() const
	{
		return BoostTankCapacity;
	}

	/**
	 * Get ship's boost depletion rate.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Boosting")
	inline float GetBoostDepletionRate() const
	{
		return BoostDepletionRate;
	}

	/**
	 * Get ship's boost restore rate.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Boosting")
	inline float GetBoostRestoreRate() const
	{
		return BoostRestoreRate;
	}

	UFUNCTION(BlueprintGetter, Category = "Ship|Info|Boosting")
	inline float GetCurrentBoostAmount() const
	{
		return m_CurrentBoostAmount;
	}

	/**
	 * Get ship's input data.
	 * This is the final combined data with user input and ship systems input.
	 */
	UFUNCTION(BlueprintGetter, Category = "Ship|Inputs")
	inline FShipInput GetShipInput() const
	{
		return m_ShipInput;
	}

	#pragma endregion

protected:

	/* Components */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	UStaticMeshComponent* Mesh;

	/* TODO: Third Person Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	USpringArmComponent* CameraArm;
	*/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	UCameraComponent* CockpitCamera;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, BlueprintGetter = GetCriticalSpeed, Category = "Critical Speed", meta = (DisplayName = "Critical Speed (Km/h)"))
	float CriticalSpeed;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, BlueprintGetter = GetBoostTankCapacity, Category = "Boosting")
	float BoostTankCapacity;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, BlueprintGetter = GetBoostDepletionRate, Category = "Boosting")
	float BoostDepletionRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, BlueprintGetter = GetBoostRestoreRate, Category = "Boosting")
	float BoostRestoreRate;


	UPROPERTY(EditAnywhere, Category = "HUD")
	TSubclassOf<UShipHUD> ShipHUD_Class;
	UShipHUD* ShipHUD;


	virtual void BeginPlay() override;

	
	/**
	 * Event called when it is time to apply inputs to thrusters.
	 * It should be implemented by the ship blueprint.
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ship|Events")
	void OnSetThrusterInputs();

	/**
	 * Helper function to set thrust input of a thruster.
	 * 
	 * @param thurster Target thruster.
	 * @param amount Amount of input to set the thruster. The input value will be automatically clamped between 0 and 1.
	 */
	UFUNCTION(BlueprintCallable, Category = "Ship|Functions")
	void SetThrustInput(UShipThrusterComponent* thurster, float amount);

private:

	static constexpr float kMpsToKph = 3.6f;

	static constexpr float kBrakingEaseOffSpeed = 10.f;

	static constexpr float kDefaultCriticalSpeed = 500.f;

	static constexpr float kDefaultBoostTankCapacity = 1000.f;
	static constexpr float kDefaultBoostDepletionRate = 50.f;
	static constexpr float kDefaultBoostRestoreRate = 25.f;


	UPrimitiveComponent* m_PhysicsBody;


	UPROPERTY(BlueprintGetter = GetShipVelocity, meta = (DisplayName = "Velocity"))
	FVector m_Velocity;

	UPROPERTY(BlueprintGetter = GetShipAngularVelocity, meta = (DisplayName = "Angular Velocity"))
	FVector m_AngularVelocity;

	UPROPERTY(BlueprintGetter = GetSpeedKph, meta = (DisplayName = "Speed (Km/h)"))
	float m_SpeedKph;
	
	UPROPERTY(BlueprintGetter = GetSpeedMps, meta = (DisplayName = "Speed (m/s)"))
	float m_SpeedMps;


	UPROPERTY(BlueprintGetter = GetCurrentBoostAmount, meta = (DisplayName = "Current Boost Ammount"))
	float m_CurrentBoostAmount;


	FShipInput m_PlayerInput;
	FShipInput m_SystemsInput;

	UPROPERTY(BlueprintGetter = GetShipInput, meta = (DisplayName = "Ship Input"))
	FShipInput m_ShipInput;


	bool m_bIsBraking;
	
	bool m_bIsBoostInputOn;
	bool m_bIsBoosting;

	bool m_bDisablePlayerInput;
	

	void CalculateShipStats();
	
	void Braking();

	void Boosting(const float deltaTime);

	UFUNCTION()
	void OnShipHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit);

	UFUNCTION()
	void FinishLineCrossed();

	void UpdateHUD() const;

	#pragma region Input Callbacks

	void BrakePressed();

	void BrakeReleased();

	void ToggleSpeedLimiterPressed();

	void BoostPressed();

	void BoostReleased();

	void RestartPressed();

	void StrafeForwardInput(float value);

	void StrafeRightInput(float value);

	void StrafeUpInput(float value);

	void YawInput(float value);
	
	void PitchInput(float value);
	
	void RollInput(float value);

	void SpeedLimiterInput(float value);

	#pragma endregion
	
	void PrintDebugStats() const;
};
