#include "TimeAttackGameMode.h"

#include "Spaceships/RaceCheckpoint.h"

ATimeAttackGameMode::ATimeAttackGameMode()
	: m_StartCheckpoint(nullptr), m_CurrentCheckpoint(nullptr), m_CheckpointCount(0), m_CurrentCheckpointIndex(0), m_Timer(Timer())
{
}

void ATimeAttackGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	m_Timer.Tick(deltaTime);
}

void ATimeAttackGameMode::SetStartCheckpoint(ARaceCheckpoint* checkpoint)
{
	if (m_StartCheckpoint != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Trying to set a start checkpoint twice. Make sure only one checkpoint is marked as active on start"));
		// Should this return the function here? Maybe... but I like to watch the world burn :)
	}

	m_StartCheckpoint = checkpoint;
	m_CurrentCheckpoint = checkpoint;

	m_CheckpointCount = CalculateCheckpointCount(m_StartCheckpoint);

	SetCurrentCheckpointIndex(1);
}

void ATimeAttackGameMode::CheckpointPassed(ARaceCheckpoint* checkpoint)
{
	checkpoint->SetActiveState(false);

	ARaceCheckpoint* nextCheckpoint = checkpoint->GetNextCheckpoint();

	if (nextCheckpoint != nullptr)
	{
		m_CurrentCheckpoint = nextCheckpoint;
		nextCheckpoint->SetActiveState(true);
	}
	else
	{
		// Finish line crossed
		m_Timer.Stop();
		OnFinishLineCrossed.Broadcast();
	}

	int checkpointsLeft = CalculateCheckpointCount(m_CurrentCheckpoint);

	SetCurrentCheckpointIndex(m_CheckpointCount - checkpointsLeft + 1);
}

int ATimeAttackGameMode::CalculateCheckpointCount(ARaceCheckpoint* startPoint)
{
	ARaceCheckpoint* current = startPoint;
	int count = 0;

	while (current != nullptr)
	{
		count++;
		current = current->GetNextCheckpoint();
	}

	return count;
}

void ATimeAttackGameMode::SetCurrentCheckpointIndex(int newValue)
{
	m_CurrentCheckpointIndex = newValue;

	OnCurrentCheckpointIndexChanged.Broadcast();
}
