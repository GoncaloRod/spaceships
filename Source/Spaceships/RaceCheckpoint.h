#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Components/SphereComponent.h"

#include "Spaceships/TimeAttackGameMode.h"

#include "RaceCheckpoint.generated.h"

UCLASS()
class SPACESHIPS_API ARaceCheckpoint : public AActor
{
	GENERATED_BODY()
	
public:	

	ARaceCheckpoint();


	void SetActiveState(bool newState);

	UFUNCTION(BlueprintCallable)
	inline bool GetActiveState() const
	{
		return m_bIsActive;
	}

	inline ARaceCheckpoint* GetNextCheckpoint() const
	{
		return NextCheckpoint;
	}

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USphereComponent* Trigger;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checkpoint|Settings")
	ARaceCheckpoint* NextCheckpoint;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checkpoint|Settings")
	bool bActiveOnStart;


	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Checkpoint|Events")
	void OnActiveStateChanged(bool newState);

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* overlappedComp, AActor* other, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult);

private:

	bool m_bIsActive;

	ATimeAttackGameMode* m_GameMode;

};
