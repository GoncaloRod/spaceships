﻿#pragma once

class Timer
{
	
public:

	Timer();
	~Timer() = default;

	void Start();

	void Stop();

	void Reset();

	void Tick(float deltaTime);


	inline int GetMilliseconds() const
	{
		return static_cast<int>(m_Seconds * 1000) % 1000;
	}

	inline int GetSeconds() const
	{
		return static_cast<int>(m_Seconds) % 60;
	}

	inline int GetMinutes() const
	{
		return static_cast<int>(m_Seconds) / 60;
	}

private:

	bool m_bRunning;

	float m_Seconds;
};
