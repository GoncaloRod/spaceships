﻿#pragma once

#include "CoreMinimal.h"

#include "Kismet/BlueprintFunctionLibrary.h"

#include "ShipInput.generated.h"

USTRUCT(BlueprintType)
struct FShipInput
{
	GENERATED_BODY()

public:

	FVector StrafeInputVector;
	FVector RotationInputVector;

	/**
	 * Resets all the input vectors back to (0, 0, 0).
	 */
	inline void Reset()
	{
		StrafeInputVector = FVector::Zero();
		RotationInputVector = FVector::Zero();
	}

	/**
	 * Adds other input data vectors to this.
	 */
	inline void Add(const FShipInput& other)
	{
		StrafeInputVector += other.StrafeInputVector;
		RotationInputVector += other.RotationInputVector;
	}
	
};

UCLASS()
class SPACESHIPS_API UShipInputBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetStrafeForwardInput(const FShipInput& input)
	{
		return input.StrafeInputVector.X > 0.f ? input.StrafeInputVector.X : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetStrafeBackwardInput(const FShipInput& input)
	{
		return input.StrafeInputVector.X < 0.f ? -input.StrafeInputVector.X : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetStrafeRightInput(const FShipInput& input)
	{
		return input.StrafeInputVector.Y > 0.f ? input.StrafeInputVector.Y : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetStrafeLeftInput(const FShipInput& input)
	{
		return input.StrafeInputVector.Y < 0.f ? -input.StrafeInputVector.Y : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetStrafeUpInput(const FShipInput& input)
	{
		return input.StrafeInputVector.Z > 0.f ? input.StrafeInputVector.Z : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetStrafeDownInput(const FShipInput& input)
	{
		return input.StrafeInputVector.Z < 0.f ? -input.StrafeInputVector.Z : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetYawRightInput(const FShipInput& input)
	{
		return input.RotationInputVector.Z > 0.f ? input.RotationInputVector.Z : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetYawLeftInput(const FShipInput& input)
	{
		return input.RotationInputVector.Z < 0.f ? -input.RotationInputVector.Z : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetPitchUpInput(const FShipInput& input)
	{
		return input.RotationInputVector.Y > 0.f ? input.RotationInputVector.Y : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetPitchDownInput(const FShipInput& input)
	{
		return input.RotationInputVector.Y < 0.f ? -input.RotationInputVector.Y : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetRollRightInput(const FShipInput& input)
	{
		return input.RotationInputVector.X > 0.f ? input.RotationInputVector.X : 0.f;
	}

	UFUNCTION(BlueprintPure, Category = "Ship Input")
	static inline float GetRollLeftInput(const FShipInput& input)
	{
		return input.RotationInputVector.X < 0.f ? -input.RotationInputVector.X : 0.f;
	}
};
