#pragma once

#include "CoreMinimal.h"
#include "Timer.h"

#include "GameFramework/GameMode.h"

#include "TimeAttackGameMode.generated.h"

class ARaceCheckpoint;

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCurrentCheckpointIndexChanged);

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFinishLineCrossed);

UCLASS()
class SPACESHIPS_API ATimeAttackGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	ATimeAttackGameMode();


	UPROPERTY(BlueprintAssignable)
	FOnCurrentCheckpointIndexChanged OnCurrentCheckpointIndexChanged;

	UPROPERTY(BlueprintAssignable)
	FOnFinishLineCrossed OnFinishLineCrossed;


	virtual void Tick(float deltaTime) override;
	
	
	void SetStartCheckpoint(ARaceCheckpoint* checkpoint);

	void CheckpointPassed(ARaceCheckpoint* checkpoint);
	

	UFUNCTION(BlueprintGetter)
	inline int GetCheckpointCount() const
	{
		return m_CheckpointCount;
	}

	UFUNCTION(BlueprintGetter)
	inline int GetCurrentCheckpointIndex() const
	{
		return m_CurrentCheckpointIndex;
	}

	UFUNCTION(BlueprintCallable)
	inline void StartTimer()
	{
		m_Timer.Start();
	}

	UFUNCTION(BlueprintCallable)
	inline int GetTimerMilliseconds() const
	{
		return m_Timer.GetMilliseconds();
	}

	UFUNCTION(BlueprintCallable)
	inline int GetTimerSeconds() const
	{
		return m_Timer.GetSeconds();
	}

	UFUNCTION(BlueprintCallable)
	inline int GetTimerMinutes() const
	{
		return m_Timer.GetMinutes();
	}

private:
	
	ARaceCheckpoint* m_StartCheckpoint;
	ARaceCheckpoint* m_CurrentCheckpoint;
	
	
	UPROPERTY(BlueprintGetter = GetCheckpointCount, meta = (DisplayName = "Checkpoint Count"))
	int m_CheckpointCount;

	UPROPERTY(BlueprintGetter = GetCurrentCheckpointIndex, meta = (DisplayName = "Current Checkpoint Index"))
	int m_CurrentCheckpointIndex;


	Timer m_Timer;

	
	static int CalculateCheckpointCount(ARaceCheckpoint* startPoint);

	void SetCurrentCheckpointIndex(int newValue);
	
};
