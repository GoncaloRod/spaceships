#pragma once

#include "CoreMinimal.h"

#include "Components/SceneComponent.h"

#include "CenterOfMassComponent.generated.h"


UCLASS(ClassGroup=(ShipComponents), meta=(BlueprintSpawnableComponent))
class SPACESHIPS_API UCenterOfMassComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	
	UCenterOfMassComponent();

protected:
	
	virtual void BeginPlay() override;

private:

	UPrimitiveComponent* m_PhysicsBody;

};
