#include "ShipThrusterComponent.h"

#include "Components/AudioComponent.h"

UShipThrusterComponent::UShipThrusterComponent()
	: bReceivesBoost(true)
{
	PrimaryComponentTick.bCanEverTick = true;

	UActorComponent::SetAutoActivate(true);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio Source"));

	AudioComponent->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);

	AudioComponent->SetAutoActivate(true);
}

void UShipThrusterComponent::BeginPlay()
{
	Super::BeginPlay();

	m_PhysicsBody = Cast<UPrimitiveComponent>(GetAttachParent());

	if (m_PhysicsBody == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Unable to find primitive component in %s"), *GetAttachParent()->GetName());
		
		SetActive(false);
	}

	// Cache thrust forces from the Data Asset
	StandardThrustForce = ThrusterData->StandardThrustForce;
	BoostingThrustForce = ThrusterData->BoostingThrustForce;

	// Apply sound cue
	AudioComponent->SetSound(ThrusterData->SoundCue);
	AudioComponent->Play();
}

void UShipThrusterComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!IsActive())
		return;

	// Update audio
	m_AudioInput = FMath::Lerp(m_AudioInput, m_Input, DeltaTime);
	AudioComponent->SetFloatParameter("Input", m_AudioInput);

	if (m_Input < k_MinimumInput)
		return;

	// Apply force to parent
	float thrustForce = m_bBoosting && bReceivesBoost ? BoostingThrustForce : StandardThrustForce;
	
	FVector force = GetComponentTransform().TransformVectorNoScale(FVector(-1.f, 0.f, 0.f)) * (thrustForce * m_Input);

	m_PhysicsBody->AddForceAtLocation(force, GetComponentLocation());
}
