// Copyright Epic Games, Inc. All Rights Reserved.

#include "Spaceships.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Spaceships, "Spaceships" );
