﻿#include "Timer.h"

Timer::Timer() : m_bRunning(false) { }

void Timer::Start()
{
	m_bRunning = true;
}

void Timer::Stop()
{
	m_bRunning = false;
}

void Timer::Reset()
{
	m_Seconds = 0.f;
}

void Timer::Tick(float deltaTime)
{
	if (!m_bRunning)
		return;

	m_Seconds += deltaTime;
}
