#pragma once

#include "CoreMinimal.h"

#include "Blueprint/UserWidget.h"

#include "ShipHUD.generated.h"

/**
 * 
 */
UCLASS()
class SPACESHIPS_API UShipHUD : public UUserWidget
{
	GENERATED_BODY()

public:

	inline void SetSpeed(const float value)
	{
		m_SpeedKph = value;
	}

	inline void SetCriticalSpeed(const float value)
	{
		m_CriticalSpeed = value;
	}

	inline void SetBoostTankCapacity(const float value)
	{
		m_BoostTankCapacity = value;
	}

	inline void SetCurrentBoostAmount(const float value)
	{
		m_CurrentBoostAmount = value;
	}

protected:

	/**
	 * Get ship's speed in Km/h.
	 */
	UFUNCTION(BlueprintGetter)
	inline float GetSpeed() const
	{
		return m_SpeedKph;
	}

	/**
	 * Get ship's critical speed.
	 */
	UFUNCTION(BlueprintGetter)
	inline float GetCriticalSpeed() const
	{
		return m_CriticalSpeed;
	}

	/**
	 * Get maximum capacity of the boost tank.
	 */
	UFUNCTION(BlueprintGetter)
	inline float GetBoostTankCapacity() const
	{
		return m_BoostTankCapacity;
	}

	/**
	 * Get current amount of boost in the boost tank.
	 */
	UFUNCTION(BlueprintGetter)
	inline float GetCurrentBoostAmount() const
	{
		return m_CurrentBoostAmount;
	}

private:

	UPROPERTY(BlueprintGetter = GetSpeed, meta = (DisplayName = "Speed"))
	float m_SpeedKph;

	UPROPERTY(BlueprintGetter = GetCriticalSpeed, meta = (DisplayName = "Critical Speed"))
	float m_CriticalSpeed;

	UPROPERTY(BlueprintGetter = GetBoostTankCapacity, meta = (DisplayName = "Boost Tank Capacity"))
	float m_BoostTankCapacity;

	UPROPERTY(BlueprintGetter = GetCurrentBoostAmount, meta = (DisplayName = "Current Boost Amount"))
	float m_CurrentBoostAmount;
};
