#include "Ship.h"

#include "TimeAttackGameMode.h"
#include "Blueprint/UserWidget.h"

AShip::AShip()
	: CriticalSpeed(kDefaultCriticalSpeed), BoostTankCapacity(kDefaultBoostTankCapacity), BoostDepletionRate(kDefaultBoostDepletionRate),
		BoostRestoreRate(kDefaultBoostDepletionRate), m_bIsBraking(false), m_bIsBoostInputOn(false), m_bIsBoosting(false)
{
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

	Mesh->SetSimulatePhysics(true);
	Mesh->SetEnableGravity(false);

	Mesh->SetNotifyRigidBodyCollision(true);
	
	Mesh->OnComponentHit.AddDynamic(this, &AShip::OnShipHit);

	RootComponent = Mesh;

	/*
	CameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Arm"));
	CameraArm->SetupAttachment(Mesh);

	CameraArm->TargetArmLength = 1000.f;
	CameraArm->bUsePawnControlRotation = false;
	*/

	CockpitCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Cockpit Camera"));
	CockpitCamera->SetupAttachment(Mesh);

	//CockpitCamera->bUsePawnControlRotation = false;

	m_PhysicsBody = FindComponentByClass<UPrimitiveComponent>();
}

void AShip::BeginPlay()
{
	Super::BeginPlay();

	// Chaos Physics Engine doesn't awake an object with angular velocity.
	// This causes the ship to not rotate properly when stopped.
	// There is no setting to toggle the sleep state of an object (as far as I know)
	// but there is a console command to disable de sleep of the physics engine.
	// This line runs the command but it's just a temporary work around.
	GEngine->Exec(GetWorld(), TEXT("p.Chaos.Solver.SleepEnabled 0"));

	GetWorld()->GetAuthGameMode<ATimeAttackGameMode>()->OnFinishLineCrossed.AddDynamic(this, &AShip::AShip::FinishLineCrossed);
	
	m_CurrentBoostAmount = BoostTankCapacity;

	if (ShipHUD_Class != nullptr)
	{
		ShipHUD = CreateWidget<UShipHUD>(GetWorld(), ShipHUD_Class);
		ShipHUD->AddToViewport();

		ShipHUD->SetCriticalSpeed(CriticalSpeed);
		ShipHUD->SetBoostTankCapacity(BoostTankCapacity);
	}
}

void AShip::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	CalculateShipStats();

	Braking();

	Boosting(deltaTime);

	// TODO: Add functions to do this
	m_ShipInput.Reset();

	if (!m_bDisablePlayerInput)
		m_ShipInput.Add(m_PlayerInput);
	
	m_ShipInput.Add(m_SystemsInput);

	OnSetThrusterInputs();

	UpdateHUD();

	PrintDebugStats();
}

void AShip::SetThrustInput(UShipThrusterComponent* thruster, float amount)
{
	amount = FMath::Clamp(amount, 0.f, 1.f);

	thruster->SetThrustInput(amount, m_bIsBoosting);
}

void AShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("Brake"), IE_Pressed, this, &AShip::BrakePressed);
	PlayerInputComponent->BindAction(TEXT("Brake"), IE_Released, this, &AShip::BrakeReleased);

	PlayerInputComponent->BindAction(TEXT("Boost"), IE_Pressed, this, &AShip::BoostPressed);
	PlayerInputComponent->BindAction(TEXT("Boost"), IE_Released, this, &AShip::BoostReleased);

	PlayerInputComponent->BindAction(TEXT("Restart"), IE_Pressed, this, &AShip::RestartPressed);

	PlayerInputComponent->BindAxis(TEXT("StrafeForward"), this, &AShip::StrafeForwardInput);
	PlayerInputComponent->BindAxis(TEXT("StrafeRight"), this, &AShip::StrafeRightInput);
	PlayerInputComponent->BindAxis(TEXT("StrafeUp"), this, &AShip::StrafeUpInput);

	PlayerInputComponent->BindAxis(TEXT("Yaw"), this, &AShip::YawInput);
	PlayerInputComponent->BindAxis(TEXT("Pitch"), this, &AShip::PitchInput);
	PlayerInputComponent->BindAxis(TEXT("Roll"), this, &AShip::RollInput);
}

void AShip::CalculateShipStats()
{
	FVector worldVelocity = m_PhysicsBody->GetPhysicsLinearVelocity();

	// Transform velocity from world space to local space
	m_Velocity = RootComponent->GetComponentTransform().InverseTransformVector(worldVelocity);

	// Change velocity from cm/s to m/s
	m_Velocity /= 100.f;

	m_SpeedMps = m_Velocity.Length();
	m_SpeedKph = m_SpeedMps * kMpsToKph;

	FVector worldAngularVelocity = m_PhysicsBody->GetPhysicsAngularVelocityInDegrees();

	// Transform angular velocity from world space to local space
	m_AngularVelocity = RootComponent->GetComponentTransform().InverseTransformVector(worldAngularVelocity);
}

void AShip::Braking()
{
	if (!m_bIsBraking)
	{
		m_SystemsInput.StrafeInputVector = FVector::Zero();
		return;
	}
	
	FVector velocity = m_Velocity;

	// Only normalize velocity vector if speed is greater then kBrakingEaseOffSpeed (10 Km/h)
	if (m_SpeedKph > kBrakingEaseOffSpeed)
	{
		velocity /= m_SpeedMps;
	}
	else
	{
		// This speed is lower then kBrakingEaseOffSpeed
		// Convert 0 .. kBrakingEaseOffSpeed -> 0 .. 1
		velocity /= kBrakingEaseOffSpeed;
	}
	
	m_SystemsInput.StrafeInputVector = -velocity;
}

void AShip::Boosting(const float deltaTime)
{
	if (m_bIsBoostInputOn)
	{
		float boostCost = BoostDepletionRate * deltaTime;

		if (boostCost > m_CurrentBoostAmount)
		{
			m_bIsBoostInputOn = false;
			m_bIsBoosting = false;
			return;
		}

		m_CurrentBoostAmount -= boostCost;
		m_bIsBoosting = true;
	}
	else
	{
		m_bIsBoosting = false;
		
		if (m_CurrentBoostAmount >= BoostTankCapacity)
			return;
		
		float boostIncrement = BoostRestoreRate * deltaTime;

		m_CurrentBoostAmount += boostIncrement;

		m_CurrentBoostAmount = FMath::Clamp(m_CurrentBoostAmount ,0.f, BoostTankCapacity);
	}
}

void AShip::OnShipHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	float hitForce = normalImpulse.Length() / 100.f;
}

void AShip::FinishLineCrossed()
{
	m_bDisablePlayerInput = true;

	m_bIsBraking = true;
	m_bIsBoostInputOn = false;
}

void AShip::UpdateHUD() const
{
	if (ShipHUD == nullptr)
		return;
	
	ShipHUD->SetSpeed(m_SpeedKph);
	ShipHUD->SetCurrentBoostAmount(m_CurrentBoostAmount);
}

#pragma region Input Callbacks

void AShip::BrakePressed()
{
	m_bIsBraking = true;
}

void AShip::BrakeReleased()
{
	m_bIsBraking = false;
}

void AShip::BoostPressed()
{
	m_bIsBoostInputOn = true;
}

void AShip::BoostReleased()
{
	m_bIsBoostInputOn = false;
}

void AShip::RestartPressed()
{
	// Not caching this for simplicity and because this isn't running every frame
	// so it's probably not a big deal. I need to find a better way to "restart" anyway...
	GetWorld()->GetAuthGameMode<AGameMode>()->RestartGame();
}

void AShip::StrafeForwardInput(float value)
{
	value = FMath::Clamp(value, -1.f, 1.f);

	m_PlayerInput.StrafeInputVector.X = value;
}

void AShip::StrafeRightInput(float value)
{
	value = FMath::Clamp(value, -1.f, 1.f);

	m_PlayerInput.StrafeInputVector.Y = value;
}

void AShip::StrafeUpInput(float value)
{
	value = FMath::Clamp(value, -1.f, 1.f);

	m_PlayerInput.StrafeInputVector.Z = value;
}

void AShip::YawInput(float value)
{
	value = FMath::Clamp(value, -1.f, 1.f);

	m_PlayerInput.RotationInputVector.Z = value;
}

void AShip::PitchInput(float value)
{
	value = FMath::Clamp(value, -1.f, 1.f);

	m_PlayerInput.RotationInputVector.Y = value;
}

void AShip::RollInput(float value)
{
	value = FMath::Clamp(value, -1.f, 1.f);

	m_PlayerInput.RotationInputVector.X = value;
}

#pragma endregion

void AShip::PrintDebugStats() const
{
	GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Cyan, FString::Printf(TEXT("Speed: %.2f Km/h"), m_SpeedKph), false);
	GEngine->AddOnScreenDebugMessage(2, 1.f, FColor::Cyan, FString::Printf(TEXT("Speed: %.2f m/s"), m_SpeedMps), false);

	GEngine->AddOnScreenDebugMessage(4, 1.f, FColor::Cyan, FString::Printf(TEXT("Angular Velocity: %s"), *m_AngularVelocity.ToString()), false);

	GEngine->AddOnScreenDebugMessage(5, 1.f, FColor::Yellow, FString::Printf(TEXT("Player Input: %s | %s"), *m_PlayerInput.StrafeInputVector.ToString(), *m_PlayerInput.RotationInputVector.ToString()), false);
	GEngine->AddOnScreenDebugMessage(6, 1.f, FColor::Yellow, FString::Printf(TEXT("Systems Input: %s | %s"), *m_SystemsInput.StrafeInputVector.ToString(), *m_SystemsInput.RotationInputVector.ToString()), false);
	GEngine->AddOnScreenDebugMessage(7, 1.f, m_bIsBraking ? FColor::Red : FColor::Yellow, FString::Printf(TEXT("Breaking: %s"), m_bIsBraking ? TEXT("True") : TEXT("False")), false);
	GEngine->AddOnScreenDebugMessage(8, 1.f, m_bIsBoostInputOn ? FColor::Cyan : FColor::Yellow, FString::Printf(TEXT("Boost Input: %s"), m_bIsBoostInputOn ? TEXT("True") : TEXT("False")), false);
}
