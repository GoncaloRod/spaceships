#pragma once

#include "CoreMinimal.h"
#include "ThrusterDataAsset.h"

#include "Components/SceneComponent.h"

#include "ShipThrusterComponent.generated.h"


UCLASS(ClassGroup=(ShipComponents), meta=(BlueprintSpawnableComponent))
class SPACESHIPS_API UShipThrusterComponent : public USceneComponent
{
	GENERATED_BODY()

public:	

	UShipThrusterComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**
	 * Set thrust input value for this thruster.
	 * The value will be automatically clamped between 0 and 1.
	 * 
	 * @param value Thrust input amount.
	 * @param boosting Should this thruster use boost.
	 */
	inline void SetThrustInput(float value, bool boosting)
	{
		value = FMath::Clamp(value, 0.f, 1.f);

		m_Input = value;
		m_bBoosting = boosting;
	}
	
	/**
	 * Get current thrust input amount.
	 * 
	 * @return Thrust input amount.
	 */
	inline float GetThrustInput() const
	{
		return m_Input;
	}

protected:

	UPROPERTY(EditAnywhere, Category = "Thruster Settings")
	UThrusterDataAsset* ThrusterData;

	UPROPERTY(EditAnywhere, Category = "Thruster Settings")
	bool bReceivesBoost;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Audio")
	UAudioComponent* AudioComponent;

	virtual void BeginPlay() override;

private:

	static constexpr float k_MinimumInput = 0.01f;

	UPrimitiveComponent* m_PhysicsBody;

	float m_Input;
	float m_AudioInput;
	bool m_bBoosting;

	float StandardThrustForce;
	float BoostingThrustForce;
};
