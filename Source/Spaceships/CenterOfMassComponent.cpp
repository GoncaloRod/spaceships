#include "CenterOfMassComponent.h"

UCenterOfMassComponent::UCenterOfMassComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UCenterOfMassComponent::BeginPlay()
{
	Super::BeginPlay();

	m_PhysicsBody = Cast<UPrimitiveComponent>(GetAttachParent());

	if (m_PhysicsBody == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Unable to find primitive component in %s"), *GetAttachParent()->GetName());

		SetActive(false);
	}

	m_PhysicsBody->SetCenterOfMass(GetRelativeLocation());
}
